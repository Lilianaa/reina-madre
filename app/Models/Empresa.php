<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;

    protected $table = 'empresas';

    protected $fillable = ['nombre'];

    //Scopes
    public function scopeOfSearch($query, $param)
    {
        if (!empty($param)) {
            return $query ->where('nombre', 'like', $param . '%');
        }
    }
}
