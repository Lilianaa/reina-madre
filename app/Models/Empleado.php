<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    use HasFactory;

    protected $table = 'empleados';

    protected $fillable = 
    [
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'fecha_nacimiento',
        'correo_electronico',
        'genero',
        'telefono',
        'celular',
        'fecha_ingreso',
        'empresa_id',
        'departamento_id'
    ];

    //Scopes
    public function scopeOfSearch($query, $param)
    {
        if (!empty($param)) {
            return $query ->where('nombre', 'like', $param . '%')
                          ->orWhere('apellido_paterno', 'like', $param . '%')
                          ->orWhere('apellido_materno', 'like', $param . '%');
        }
    }

    //Relaciones
    public function departamento()
    {
        return $this->belongsTo(Departamento::class);
    }

    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }
}
