<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RolRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validacion = [];

        switch ($this->method()) {
            case 'POST':
                $validacion['nombre'] = 'required|unique:roles,nombre';
                break;

            case 'PUT':
                $validacion['nombre'] = 'required|'.Rule::unique('roles')->ignore($this->id);
                break;  
        }

        return $validacion;
    }
}
