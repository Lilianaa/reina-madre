<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class EmpleadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validacion = [
            'nombre'     => 'required',
            'apellido_paterno' => 'required',
            'apellido_materno' => 'required',
            'telefono' => 'nullable',
            'celular' => 'nullable',
            'fecha_nacimiento' => 'required',
            'fecha_ingreso' => 'required',
            'empresa_id' => 'required',
            'departamento_id' => 'required'
        ];

        switch($this->method()){
            case 'POST':
            $validacion['correo_electronico'] = 'required|email|unique:empleados,correo_electronico';
            break;

            case 'PUT':
            $validacion['correo_electronico'] = 'required|email|' .Rule::unique('empleados')->ignore($this->id);
            break;
        }

        return $validacion;
    }
}
