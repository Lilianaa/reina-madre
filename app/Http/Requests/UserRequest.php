<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validacion = [
            'name'     => 'required',
            'rol_id'   => 'required',
            'password' => 'required',
        ];

        switch($this->method()){
            case 'POST':
            $validacion['email'] = 'required|email|unique:users,email';
            break;

            case 'PUT':
            $validacion['email'] = 'required|email|nullable|' .Rule::unique('users')->ignore($this->id);
            break;
        }

        return $validacion;
    }
}
