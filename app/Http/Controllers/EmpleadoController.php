<?php

namespace App\Http\Controllers;

use App\Models\Empleado;
use Illuminate\Http\Request;
use App\Http\Requests\EmpleadoRequest;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->wantsJson())
        {
            $empleados   = [];
            $departamento_id = $request->departamento_id;
            $empresa_id  = $request->empresa_id;

            $sort = isset($request->sort) ? explode("|", $request->sort) : explode("|", 'id|asc');
        
            $empleados = Empleado::ofSearch($request->input('query'))
            ->when($departamento_id, function($query, $departamento_id){
                return $query->where('departamento_id', $departamento_id);
            })
            ->when($empresa_id, function($query, $empresa_id){
                return $query->where('empresa_id', $empresa_id);
            })
            ->with('departamento','empresa')
            ->orderBy($sort[0], $sort[1])
            ->paginate($request->per_page);

            return $empleados;
        }
        
        return view('empleados.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empleados.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmpleadoRequest $request)
    {
        Empleado::create($request->validated());

        return response()->json([
            'mensaje' => 'Empleado creado correctamente'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empleado = Empleado::find($id);

        return response()->json($empleado);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('empleados.edit', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmpleadoRequest $request, $id)
    {
        $empleado = Empleado::find($id);
        $empleado->update($request->validated());

        return response()->json([
            'mensaje' => 'Empleado editado correctamente'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empleado = Empleado::find($id);
        $empleado->delete();
    }
}
