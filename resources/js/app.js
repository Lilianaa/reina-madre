/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Departamentos
Vue.component(
    'index-departamentos',
    require('./components/departamentos/index.vue').default
)

Vue.component(
    'form-departamentos',
    require('./components/departamentos/form.vue').default
)

//Empleados

Vue.component(
    'index-empleados',
    require('./components/empleados/index.vue').default
)

Vue.component(
    'form-empleados',
    require('./components/empleados/form.vue').default
)
//Empresas
 Vue.component(
    'index-empresas',
    require('./components/empresas/index.vue').default
)

Vue.component(
    'form-empresas',
    require('./components/empresas/form.vue').default
)

//Roles
Vue.component(
    'index-roles',
    require('./components/roles/index.vue').default
)

Vue.component(
    'form-roles',
    require('./components/roles/form.vue').default
)

//Users
Vue.component(
    'index-users',
    require('./components/users/index.vue').default
)

Vue.component(
    'form-users',
    require('./components/users/form.vue').default
)

const app = new Vue({
    el: '#app',
});
