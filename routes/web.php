<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\{
    EmpresaController,
    DepartamentoController,
    EmpleadoController,
    RolController,
    UserController

};


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/login');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group([
    'middleware' => ['auth']
], function () {
    //empresas
    Route::resource('empresas', EmpresaController::class);

    //departamentos
    Route::resource('departamentos', DepartamentoController::class);

    //empleados
    Route::resource('empleados', EmpleadoController::class);

    //Roles
    Route::resource('roles', RolController::class);

    //Usuarios
    Route::resource('users', UserController::class);
});

