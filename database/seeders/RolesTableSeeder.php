<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Rol;
use Illuminate\Support\Facades\Schema;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Rol::truncate();
        Schema::enableForeignKeyConstraints();

        Rol::create(['nombre' => 'Admin']);
        Rol::create(['nombre' => 'Empleado']);
    }
}
