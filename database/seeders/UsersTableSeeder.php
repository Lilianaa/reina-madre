<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
            'name'         => 'Reina Madre',
            'email'        => 'reina.madre@hotmail.com',
            'password'     => Hash::make('1234567890'),
            'rol_id'       => 1
        ]);

        User::create([
            'name'         => 'Liliana Peralta Palma',
            'email'        => 'liliana.peralta25@hotmail.com',
            'password'     => Hash::make('1234567890'),
            'rol_id'       => 2
        ]);
    }
}
